#ifndef CANETA_HPP
#define CANETA_HPP

#include <string>

class Caneta {
   private:	
	// Atributos
	std::string nome;
	std::string cor;  
	std::string marca;
	float preco;
   public:
	// métodos
	Caneta(); // Método Construtor
	~Caneta();// Método Destrutor
	// Métodos acessores
	std::string getNome();
	void setNome(std::string nome);
	std::string getCor();
	void setCor(std::string cor);
	std::string getMarca();
	void setMarca(std::string marca);
	float getPreco();
	void setPreco(float preco);
	// Outros Métodos
	void escrever(float comprimento);
	void destampar();

};

#endif

